# LiFePo Charger

A charger for LiFePo4 batteries. Built after the design found at [Engineering Projects](https://bestengineeringprojects.com/lifepo4-battery-charger-circuit/). This circuit will charge LiFePo4 batteries to approximately 3.6V. At the core of the design is an OpAmp. This OpAmp gets a reference volatage input and the voltage from the battery as the other input. It operates as a comparator, thus the output of the OpAmp is larger if the difference of the inputs is larger. As the battery charges up the difference gets smaller and thus the loading current decreases. This leads to the desired profile of loading LiFePo4 batteries with less current when the maximum charge is approached. However, as the output of the OpAmp continously decreases until there is no more difference with the input, charging the battery fully takes quite long.

Note that this circuit is rather educational and a good excercise in soldering if you are new to the topic. Specialised charger chips are likely more efficient and also use much less space in an actual design. The circuit and the pcb are designed in [KiCAD](https://www.kicad.org/), an open source software for circuit and pcb design. You can either adapt the design to your needs or directly order a board with a pcb manufacturer. To do this use the gerber files in the ./gerber directory. Feel free to contribute any improvements and changes to this repository. 

One important remark for the proper functioning of the circuit: Depending on your LED's voltage drop you might have to adjust resistor values in order to get proper charging output (as the difference at the input of the opamp for VREF_CHG is dependent on the led for power (LED3_POWER)).

I list the BOM with exmaple sources for buying the parts. Note that the links are only an example for conveninece. Neither do I recommend the seller over others or am I affiliated in any othe way. Parts can of course be sourced wherever most convenient for you.

| Name           | Designator     | Qty | Value                  | Footprint        | Example Source | 
| -------------- | -------------- | --- | ---------------------- | ---------------- | -------------- |
| Battery Holder | BT1            | 1   | 18650                  | SMD_SMT 1x18650  | [Aliexpress](https://www.aliexpress.com/item/4000692013589.html?spm=a2g0o.productlist.0.0.18834683Amss5C&algo_pvid=c541e12f-f2aa-4ac9-9149-ab4ee91b8881&aem_p4p_detail=202110160403207598552678029320000752554&algo_exp_id=c541e12f-f2aa-4ac9-9149-ab4ee91b8881-1&pdp_ext_f=%7B%22sku_id%22%3A%2210000006043936595%22%7D) |
| USB Connector  | J1             | 1   | USBC Receptable USB2.0 | HRO-Type-C31-M12 | [LCSC](https://lcsc.com/product-detail/USB-Connectors_Korean-Hroparts-Elec-TYPE-C-31-M-12_C165948.html) |
| OpAmp          | U1             | 1   | LM358                  | SOP-8_3.9x4.9mm  | [LCSC](https://lcsc.com/product-detail/Operational-Amplifier_HGSEMI-LM358M-TR_C398078.html) |
| PNP Transistor | Q1             | 1   | S8550                  | SOT-23           | [LCSC](https://lcsc.com/product-detail/Bipolar-Transistors-BJT_JSMSEMI-S8550_C916391.html)  |
| Charging LED   | LED1           | 1   | red                    | 0805             | [Leds and More](https://www.leds-and-more.de/catalog/kingbright-smd-led-0805-diffus-rot-mcd-120-2012id-p-1733.html?osCsid=orh255ipb6a0p875g2ejlg6u75) |
| Charged LED    | LED2           | 1   | green                  | 0805             | [Leds and More](https://www.leds-and-more.de/catalog/kingbright-smd-led-0805-diffus-gruen-mcd-120-2012sgd-p-1732.html?osCsid=orh255ipb6a0p875g2ejlg6u75) |
| Power LED      | LED3           | 1   | red                    | 0805             | [Leds and More](https://www.leds-and-more.de/catalog/kingbright-smd-led-0805-diffus-rot-mcd-120-2012id-p-1733.html?osCsid=orh255ipb6a0p875g2ejlg6u75) |
| Diode          | D1             | 1   | 1N4007                 | SMA(DO-214AC)    | [LCSC](https://lcsc.com/product-detail/Diodes-General-Purpose_TWGMC-1N4007_C727081.html) |
| Capacitor      | C1             | 1   | 100nF                  | 0805             | [LCSC](https://lcsc.com/product-detail/Multilayer-Ceramic-Capacitors-MLCC-SMD-SMT_TDK-CEU4J2X7R1H104KT0Y0S_C695378.html) |
| Resistor       | R1,R4,R6,R7,R8 | 5   | 1.2 kOhm               | 2512             | [LCSC](https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_Viking-Tech-SWR12JTEA1201_C412433.html) |
| Resistor       | R2             | 1   | 560 Ohm                | 2512             | [LCSC](https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_UNI-ROYAL-Uniroyal-Elec-CQ121WF5600T4E_C966233.html) |
| Resistor       | R3             | 1   | 680 Ohm                | 2512             | [LCSC](https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_UNI-ROYAL-Uniroyal-Elec-25121WF6800T4E_C56080.html) |
| Resistor       | R5             | 1   | 2.2 kOhm               | 2512             | [LCSC](https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_Ever-Ohms-Tech-CR2512J2K20E04Z_C146937.html) |
| Resistor       | R9,R10         | 2   | 5.1 kOhm               | 2512             | [LCSC](https://lcsc.com/product-detail/Chip-Resistor-Surface-Mount_Ever-Ohms-Tech-CRH2512F5K10E04Z_C175339.html) |

A note on the footprints: Of course you can use any other footprint for the parts. I chose 2512 footprints for the resistors for two reasons. First, they are easier to solder and second, in general it is a good idea to choose larger footprints for power supply resistors. The 0805 footprints were chosen because I had the parts already in my stock. Feel free to adapt. 

Other things you might want to customise are the battery holder if you are using another size than 18650 batteries or the power source conncetion if you prefere anything other than USB-C. However, note that the circuit is designed for a 5V power source. If you change the supply voltage you need to adapt the circuit accordingly.

For detailed explanations of the circuit study the original design at https://bestengineeringprojects.com/lifepo4-battery-charger-circuit/
